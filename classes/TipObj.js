//@desc A class for all of the ifnormation needed in a tips
//@param content The content of the tip to be displayed primarily.
//@param categoreis The categories or tags for a tip. Stored as an array.
//@param confirms The number of "confirms" a tip has
//@param nopes The number of "nopes" a tip has
//@param key The key for the tip (relates to the firebase database)
export default class TipObj {
  constructor(content, categories, confirms, nopes, key) {
    this.content = content;
    this.categories = categories;
    this.confirms = confirms;
    this.nopes = nopes;
    //Allows for a constructor without providing a key
    if(typeof key != 'undefined')
      this.key = key;
  }
}
