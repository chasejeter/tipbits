// desc: The Tip component that displays the content, the tags, the number of nopes and confirms, and the save button.
// The tip can also be expanded to show the description or extra info.
// Also includes category to solve 'chicken and egg' problem.

import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView, Text, View, Modal, TextInput, TouchableHighlight, TouchableOpacity, Alert, FlatList, Button, TouchableWithoutFeedback, StatusBar } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import TipContainer from './TipContainer.js';
import Category from './Category.js';
import * as Amplitude from 'expo-analytics-amplitude';



export default class BaseTip extends React.Component {
  constructor(props) {
    super(props);
    //@value confirmed whether or not the user confirmed the tip
    //@value extra extra description
    this.state = {
      confirms: this.props.confirms || 0,
      nopes: this.props.nopes || 0,
      confirmed: this.props.confirmed || false,
      noped: this.props.noped || false,
      modalVisible: false,
      extra: this.props.extra || "",
      saved: this.props.saved || false
    }
    this._confirm = this._confirm.bind(this);
    this._nope = this._nope.bind(this);
    this._save = this._save.bind(this);
    this._modalVisible = this._modalVisible.bind(this);
  }

  // @overwrite
  async _refresh() {

  }

  componentDidMount() {
    this._refresh();
  }

  //Adds to the confirms count only if the user hasn't already confirmed.
  //Otherwise, subtracts from the confirms count

  //LATER: MAKE A confirm() FUNCTION FOR OVERWRITING
  _confirm() {
    let confirms = this.state.confirms;
    let nopes = this.state.nopes;
    let noped = this.state.noped;
    if(this.state.confirmed == true)
      confirms--;
    else {
      if(this.state.noped) {
        nopes--;
        noped = !this.state.noped;
      }
      confirms++;
    }
    this.confirm();
    this.setState((previousState) => ({confirms: confirms, confirmed: !previousState.confirmed, nopes: nopes, noped: noped}));
  }

  // @Overwrite
  confirm() {

  }

  //Adds to the noeps count only if the user hasn't already noped.
  //Otherwise, subtracts from the nopes count
  // this should really be implemented serverside
  _nope() {
    let nopes = this.state.nopes;
    let confirms = this.state.confirms;
    let confirmed = this.state.confirmed;
    if(this.state.noped == true)
      nopes--;
    else {
      if(this.state.confirmed) {
        confirms--;
        confirmed = !this.state.confirmed;
      }
      nopes++;
    }
    this.nope();
    this.setState((previousState) => ({nopes: nopes, noped: !previousState.noped, confirms: confirms, confirmed: confirmed}));
  }

  nope() {

  }

  //saves a tip for the user to view on their profile page
  _save() {
    this.save();
    this.setState((previousState) => ({saved: !previousState.saved}));
  }

  save() {

  }

  //sets the visibility of the full screen tip
  _modalVisible(tof) {
    this.modalVisible(tof);
    this.setState({modalVisible: tof});
  }

  modalVisible(tof) {

  }

  //Gets the extra text of a tip
  getExtraText() {

  }

  //deletes a tip from the database
  delete() {

  }

  render() {

    return (
      <View style={{width:'100%'}}>
        <Modal
          animationType={'fade'}
          onRequestClose={() => this._modalVisible(false)}
          visible={this.state.modalVisible}
        >
          <SafeAreaView style={{flex: 1, backgroundColor:'tomato'}}>
            <TouchableWithoutFeedback
              onPress={()=> this._modalVisible(false)}>
              <View style={{flexDirection:'row-reverse', alignItems:'center'}}>
                <View style={{flex:.95, flexDirection:"row", alignItems:'center'}}>
                  <Ionicons name={'ios-arrow-back'} size={30} color={'white'} />
                  <Text style={{color:'white', fontSize:30, marginBottom:2}}> Back</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
            <ScrollView contentContainerStyle={{alignItems:'center'}}>
              <View style={{width:'90%'}}>
                <TipContainer
                  content={this.props.content}
                  confirmed={this.state.confirmed}
                  saved={this.state.saved}
                  noped={this.state.noped}
                  deletable={this.props.deletable}
                  tags={this.props.tags}
                  extra={this.state.extra}
                  confirms={this.state.confirms}
                  nopes={this.state.nopes}
                  displayExtra={this.state.modalVisible}
                  confirm={this._confirm}
                  nope={this._nope}
                  save={this._save}
                  delete={this.delete}
                />
              </View>
            </ScrollView>
          </SafeAreaView>
        </Modal>
        <TouchableWithoutFeedback
          onPress={()=> {
            this.getExtraText();
            this._modalVisible(true);
          }}
        >
          <View>
            <TipContainer
              content={this.props.content}
              confirmed={this.state.confirmed}
              saved={this.state.saved}
              noped={this.state.noped}
              deletable={this.props.deletable}
              tags={this.props.tags}
              extra={this.state.extra}
              confirms={this.state.confirms}
              nopes={this.state.nopes}
              displayExtra={this.state.modalVisible}
              confirm={this._confirm}
              nope={this._nope}
              save={this._save}
              delete={this.delete}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  category: {
    backgroundColor: 'gray',
    color: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderRadius: 15,
    marginLeft: 3
  },
  header: {
    width: "100%",
    backgroundColor: 'white',
    paddingTop: 32.5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    marginBottom: 10,
    marginTop: -20
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    fontWeight: "bold",
  },
  tagsDisplay: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 3
  },
  container: {
    flexDirection: "row",
    justifyContent:"center"
  }
});
