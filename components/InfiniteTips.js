import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import Tip from './Tip.js';
import PropTypes from 'prop-types';
import { connectInfiniteHits } from 'react-instantsearch-native';

const styles = StyleSheet.create({
  separator: {
    borderBottomWidth: 1,
    borderColor: '#ddd',
  },
  item: {
    padding: 10,
    flexDirection: 'column',
  },
  titleText: {
    fontWeight: 'bold',
  },
});

const InfiniteTips = ({ uid, hits, hasMore, refine }) => {
  return (
    <FlatList
      keyExtractor={item => item.objectID}
      onEndReached={() => hasMore && refine()}
      refreshing={false}
      showsVerticalScrollIndicator={false}
      ListHeaderComponent={<View style={{marginTop:10}} />}
      data={hits}
      renderItem={({item}) => {
        return (
          <View style={{width:"90%", alignSelf:"center"}}>
            <Tip tags={item.categories} content={item.content} tipKey={item.objectID} uid={uid}/>
          </View>
        )
      }}
    />
  );
};

InfiniteTips.propTypes = {
  hits: PropTypes.arrayOf(PropTypes.object).isRequired,
  hasMore: PropTypes.bool.isRequired,
  refine: PropTypes.func.isRequired,
};

export default connectInfiniteHits(InfiniteTips);
