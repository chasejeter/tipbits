import React from 'react';
import { SafeAreaView,Text,View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import TipObj from '../classes/TipObj.js';
import Category from './Category.js';
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <SafeAreaView style={{backgroundColor:'white'}}>
        <View style={{paddingTop:5, paddingBottom: 7, ...this.props.style}}>
          <Text style={{fontSize: 30, fontWeight:'bold', textAlign:'left', marginHorizontal:16}}>{this.props.title}</Text>
        </View>
      </SafeAreaView>
    )
  }
}
