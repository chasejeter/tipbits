// desc: The Tip component that displays the content, the tags, the number of nopes and confirms, and the save button.
// The tip can also be expanded to show the description or extra info.
// Also includes category to solve 'chicken and egg' problem.

import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView, Text, View, Modal,  Alert, TouchableWithoutFeedback } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import TipObj from '../classes/TipObj.js';
import BaseTip from './BaseTip.js';
import Category from './Category.js';
import * as firebase from 'firebase';
require('firebase/functions');
import * as Amplitude from 'expo-analytics-amplitude';

export default class Tip extends BaseTip {
  constructor(props) {
    super(props);

    //database references
    this.confirmsRef = firebase.database().ref('tips/' + this.props.tipKey + '/confirms');
    this.nopesRef = firebase.database().ref('tips/' + this.props.tipKey + '/nopes');
    this.userRef = firebase.database().ref('users/' + this.props.uid);
    this.confirm = this.confirm.bind(this);
    //bindings
    this.nope = this.nope.bind(this);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
    this._refresh = this._refresh.bind(this);
    this.renderCount = 0;


    this.modalVisible = this.modalVisible.bind(this);
  }

  async _refresh() {

    const confirms = this.confirmsRef.once('value').then((snapshot) => {
      if(typeof snapshot.val() != 'undefined')
        return snapshot.val()
      else {
        return null
      }
    });

    const nopes = this.nopesRef.once('value').then((snapshot) => {
      return snapshot.val();
    });

    const confirmed = this.userRef.child('confirmed/' + this.props.tipKey).once('value').then((snapshot) => {
      const val = snapshot.val();
      if(val == undefined)
        return false;
      return val;
    });

    const noped = this.userRef.child('noped/' + this.props.tipKey).once('value').then((snapshot) => {
      const val = snapshot.val();
      if(val == undefined)
        return false;
      return val;
    });

    const saved = firebase.database().ref('users/' + this.props.uid + '/saved/' + this.props.tipKey).once('value').then((snapshot) => {
      const val = snapshot.val();
      if(val == undefined)
        return false;
      return val;
    });

    this.setState({confirms: await confirms, nopes: await nopes, confirmed: await confirmed, noped: await noped, saved: await saved});
  }

  //Adds to the confirms count only if the user hasn't already confirmed.
  //Otherwise, subtracts from the confirms count
  confirm() {
    const agree = firebase.functions().httpsCallable('tipActions-agree');
    if(this.state.confirmed != true)
      Amplitude.logEventWithProperties("Tip Button", {buttonType: "agree"});
    agree({tipKey:this.props.tipKey});
  }

  //Adds to the noeps count only if the user hasn't already noped.
  //Otherwise, subtracts from the nopes count
  nope() {
    const disagree = firebase.functions().httpsCallable('tipActions-disagree');
    if(this.state.noped != true)
      Amplitude.logEventWithProperties("Tip Button", {buttonType: "disagree"});
    disagree({tipKey:this.props.tipKey});
  }

  //saves a tip for the user to view on their profile page
  save() {
    if(this.state.saved == false) {
      let tip = {
        categories: this.props.tags,
        confirms: this.state.confirms,
        nopes: this.state.nopes,
        content: this.props.content
      };
      firebase.database().ref('users/' + this.props.uid + '/saved/' + this.props.tipKey).set(tip);
      Amplitude.logEventWithProperties("Tip Button", {buttonType: "save"});
    }
    else
      firebase.database().ref('users/' + this.props.uid + '/saved/' + this.props.tipKey).remove();
  }

  //sets the visibility of the full screen tip
  modalVisible(tof) {
    if(tof)
      //logs a tip press
      Amplitude.logEvent("Tip Press");
  }

  //Gets the extra text of a tip
  getExtraText() {
    firebase.database().ref('/extras/' + this.props.tipKey).once('value', (snapshot) => {
      if(snapshot.val())
        this.setState({extra: snapshot.val().extra});
    });
  }

  //deletes a tip from the database
  delete() {
    if(this.props.deletable) {
      //only deletes after the deletion from the user tips is permissable
      firebase.database().ref('/users/' + this.props.uid + '/tips/' + this.props.tipKey).remove().then(() => {
        firebase.database().ref('/tips/' + this.props.tipKey).remove();
        firebase.database().ref('/extras/' + this.props.tipKey).remove();
        for (let tag of this.props.tags) {
          firebase.database().ref('/categories/' + tag.toLowerCase() + '/' + this.props.tipKey).remove();
        }
      })
      .catch((error) => {
        //
        Alert.alert("Something went wrong.");
      });
    }
  }

}

const styles = StyleSheet.create({
  category: {
    backgroundColor: 'gray',
    color: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderRadius: 15,
    marginLeft: 3
  },
  header: {
    width: "100%",
    backgroundColor: 'white',
    paddingTop: 32.5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    marginBottom: 10,
    marginTop: -20
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    fontWeight: "bold"
  },
  tagsDisplay: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 3
  },
  container: {
    flexDirection: "row",
    justifyContent:"center"
  }
});
