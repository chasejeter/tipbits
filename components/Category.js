//Component that wraps text into a pressable, category (or tag).
import React from 'react';
import { StyleSheet, Text, View, Button, ScrollView, TouchableOpacity } from 'react-native';
import * as firebase from 'firebase';
import TutorialContext from '../contexts/TutorialContext.js';
import { withNavigation } from 'react-navigation';

class Category extends React.Component {
  //@prop name The name of the category

  static contextType = TutorialContext;

  constructor(props) {
    super(props);
    this.goToCategory = this.goToCategory.bind(this);
  }

  goToCategory() {
    if(this.props.navigation !== null && typeof this.props.navigation !== 'undefined')
      if(typeof this.props.navigation.push === 'undefined')
        this.props.navigation.navigate('Category', {name: this.props.name.toLowerCase()});
      else
        this.props.navigation.push('Category', {name: this.props.name.toLowerCase()});
      // this.props.navigation.navigate('Category', {name: this.props.name});
  }

  render() {
    //
    let onPressFunction = this.goToCategory.bind(this);
    if(this.context.notActive) {
      onPressFunction = null
    }
    return (
      <View>
        <TouchableOpacity
          onPress={onPressFunction}
        >
          <View style={styles.category}><Text style={{color:'white'}}>{this.props.name.toLowerCase()}</Text></View>
        </TouchableOpacity>
      </View>
    );
  }
}

export class CategoryList extends React.Component {
  render() {
    let tagsArray = [];
    for(let i = 0; i<this.props.tags.length; i++) {
        tagsArray.push(<Category name={this.props.tags[i]} notPressable={!this.props.pressable} key={this.props.tags[i]} />);
    }

    return (
      <View style={styles.tagsDisplay}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          { tagsArray }
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  category: {
    backgroundColor: 'gray',
    color: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderRadius: 15,
    marginLeft: 3
  },
  header: {
    width: "100%",
    backgroundColor: 'white',
    paddingTop: 32.5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    marginBottom: 10,
    marginTop: -20
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  tagsDisplay: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 3
  }
});

export default withNavigation(Category);
