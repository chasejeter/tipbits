// desc: The Tip component that displays the content, the tags, the number of nopes and confirms, and the save button.
// The tip can also be expanded to show the description or extra info.

import React from 'react';
import { StyleSheet, ScrollView, SafeAreaView, Text, View, Modal, TextInput, TouchableHighlight, Alert, Button, TouchableWithoutFeedback, StatusBar } from 'react-native';
import Category from './Category.js';
import { Ionicons } from '@expo/vector-icons';
import * as firebase from 'firebase';

export default class Tip extends React.Component {
  constructor(props) {
    super(props);
    //@value confirmed whether or not the user confirmed the tip
    //@value extra extra description
    this.state = {
      confirmed: false,
      noped: false,
      confirms: 0,
      nopes: 0,
      modalVisible: false,
      extra: "This is where extra explanation or further description goes!",
      saved: false
    }
    this.confirm = this.confirm.bind(this);
    this.nope = this.nope.bind(this);
    this.save = this.save.bind(this);
    this.modalVisible = this.modalVisible.bind(this);
  }

  //Adds to the confirms count only if the user hasn't already confirmed.
  //Otherwise, subtracts from the confirms count
  confirm() {
    this.setState(previousState => ({
      confirmed: !previousState.confirmed,
      confirms: previousState.confirmed ? 0:1,
      noped: false,
      nopes: 0
    }));
  }

  //Adds to the noeps count only if the user hasn't already noped.
  //Otherwise, subtracts from the nopes count
  nope() {
    this.setState(previousState => ({
      noped: !previousState.noped,
      nopes: previousState.noped ? 0:1,
      confirmed: false,
      confirms: 0
    }));
  }

  //saves a tip for the user to view on their profile page
  save() {
    this.setState(previousState => ({
      saved: !previousState.saved
    }));
  }

  //sets the visibility of the full screen tip
  modalVisible(tof) {
    this.setState({modalVisible: tof});
  }


  render() {
    //Changes the color of the icon depending on if the buttons have been toggled
    let confirmedColor = 'gray';
    let nopedColor = 'gray';
    let savedColor = 'gray';
    if(this.state.confirmed)
      //shade of green
      confirmedColor = '#45cc60';
    else {
      confirmedColor = 'gray';
    }
    if(this.state.saved)
      //yellow-ish
      savedColor = '#ffd338';
    else
      savedColor = 'gray';
    if(this.state.noped)
      //redish
      nopedColor = '#4c163c';
    else {
      nopedColor = 'gray';
    }
    let tagsArray = [];
    for(let i = 0; i<this.props.tags.length; i++) {
      if(this.props.tags[i])
        tagsArray.push(<Category name={this.props.tags[i]} key={this.props.tags[i]} />);
    }

    let tagsDisplay =
    <View style={styles.tagsDisplay}>
      <Text>Tags: </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        { tagsArray }
      </ScrollView>
    </View>;

    let extra = {
      fontSize: 20,
      color: "#575757",
      marginBottom: 5,
    };
    if(this.state.modalVisible === false || this.state.extra.length === 0)
      extra.display = "none";

    const tipContainer = (
          <View style={{marginBottom: 10, backgroundColor:'white', padding: 10}}>
            <Text style={styles.text}>{this.props.content}</Text>
            <Text style={ extra }>{this.state.extra}</Text>
            {
              tagsDisplay
            }
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <View style={styles.button}>
                <TouchableWithoutFeedback
                  onPress={() => {this.confirm()} }>
                  <Ionicons name={'ios-arrow-dropup-circle'} style={{marginRight: 5}} size={35} color={confirmedColor} />
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                  onPress={() => {this.nope()}}>
                  <Ionicons name={'ios-arrow-dropdown-circle'} size={35} color={nopedColor} />
                </TouchableWithoutFeedback>
                <Text style={{marginLeft: 7}}>{this.state.confirms-this.state.nopes}</Text>
              </View>
              <View style={styles.button}>
                <TouchableWithoutFeedback
                  onPress={() => {this.save()}}>
                  <Ionicons name={'ios-add-circle'} size={35} color={savedColor} />
                </TouchableWithoutFeedback>
              </View>
            </View>
          </View>
      )


    // if(this.props.content.indexOf("Due C") > -1)
    //
    return (
      <View>
        <Modal
          animationType={'fade'}
          onRequestClose={() => this.modalVisible(false)}
          visible={this.state.modalVisible}
        >
          <SafeAreaView style={{flex: 1, backgroundColor:'tomato'}}>
            <TouchableWithoutFeedback
              onPress={()=> this.modalVisible(false)}>
              <View style={{flexDirection:'row-reverse', alignItems:'center'}}>
                <View style={{flex:.95, flexDirection:"row", alignItems:'center'}}>
                  <Ionicons name={'ios-arrow-back'} size={30} color={'white'} />
                  <Text style={{color:'white', fontSize:30, marginBottom:2}}> Back</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
            <View style={styles.container}>
              <View style={{flex:.9}}>
                {tipContainer}
              </View>
            </View>
          </SafeAreaView>
        </Modal>
        <TouchableWithoutFeedback
          onPress={()=> {
            this.modalVisible(true);
          }}
        >
          <View>
            {tipContainer}
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  category: {
    backgroundColor: 'gray',
    color: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderRadius: 15,
    marginLeft: 3
  },
  header: {
    width: "100%",
    backgroundColor: 'white',
    paddingTop: 32.5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    marginBottom: 10,
    marginTop: -20
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    fontWeight: "bold"
  },
  tagsDisplay: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 3
  },
  container: {
    flexDirection: "row",
    justifyContent:"center"
  }
});
