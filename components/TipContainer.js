import React from 'react';
import { StyleSheet, SafeAreaView, ScrollView, Text, View, TouchableOpacity, Button, TouchableWithoutFeedback } from 'react-native';
import * as Haptics from 'expo-haptics';
import { Ionicons } from '@expo/vector-icons';
import Category from './Category.js';

export default class TipContainer extends React.Component {

  render() {

      //Changes the color of the icon depending on if the buttons have been toggled
      let confirmedColor = 'gray';
      let nopedColor = 'gray';
      let savedColor = 'gray';
      if(this.props.confirmed)
        //shade of green
        confirmedColor = '#45cc60';
      else {
        confirmedColor = 'gray';
      }
      if(this.props.saved)
        //yellow-ish
        savedColor = '#ffd338';
      else
        savedColor = 'gray';
      if(this.props.noped)
        //redish
        nopedColor = '#4c163c';
      else {
        nopedColor = 'gray';
      }
      let trash = null;
      if(this.props.deletable) {
        trash =
        <View style={{flexDirection:'row', justifyContent:'flex-end'}}>
          <TouchableWithoutFeedback
            onPress={this.props.delete}>
            <Ionicons name={'ios-trash'} size={20} color={'gray'} />
          </TouchableWithoutFeedback>
        </View>;
      }
      let tagsArray = [];
      for(let i = 0; i<this.props.tags.length; i++) {
        if(this.props.tags[i])
          tagsArray.push(<Category name={this.props.tags[i]} key={this.props.tags[i]} notPressable={(this.props.notActive || false)} />);
      }

      let tagsDisplay =
      <View style={styles.tagsDisplay}>
        <Text>Tags: </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          { tagsArray }
        </ScrollView>
      </View>;

      let extra = {
        fontSize: 20,
        color: "#575757",
        marginBottom: 5,
      };
      if(this.props.displayExtra === false || this.props.extra.length === 0)
        extra.display = "none";

    return (
      <View style={{marginBottom: 10, backgroundColor:'white', padding: 10, borderRadius:5, ...this.props.style}}>
        {
          trash
        }
        <Text style={styles.text}>{this.props.content}</Text>
        <Text style={ extra }>{this.props.extra}</Text>
        {/*For testing: <Text>{JSON.stringify(testObj)}</Text>*/}
        {
          tagsDisplay
        }
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View style={styles.button}>
            <TouchableWithoutFeedback
              onPress={() => {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
                  .catch((err) => { //No error message
                    return;
                  });
                this.props.confirm();
              } }>
              <Ionicons name={'ios-arrow-dropup-circle'} style={{marginRight: 5}} size={35} color={confirmedColor} />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
                  .catch((err) => { //No error message
                    return;
                  });
                this.props.nope()
              }}>
              <Ionicons name={'ios-arrow-dropdown-circle'} size={35} color={nopedColor} />
            </TouchableWithoutFeedback>
            <Text style={{marginLeft: 7}}>{this.props.confirms-this.props.nopes}</Text>
          </View>
          <View style={styles.button}>
            <TouchableWithoutFeedback
              onPress={() => {
                Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light)
                  .catch((err) => { //No error message
                    return;
                  });
                this.props.save()
              }}>
              <Ionicons name={'ios-add-circle'} size={35} color={savedColor} />
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    fontWeight: "bold",
  },
  tagsDisplay: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 3
  }
});
