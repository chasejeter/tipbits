//desc: Screen for adding tips to the database

import React, {Component} from 'react';
import {Text, ScrollView, TouchableOpacity, View, Alert, TextInput , StyleSheet, Platform} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { CategoryList } from '../components/Category.js';
import TipObj from '../classes/TipObj.js';
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';
import {NavigationActions} from 'react-navigation';
/*var config = {
    apiKey: "AIzaSyBVzi3xT38raX3b5vdCGe15zmbuevh-GX0",
    authDomain: "tipbits-67cb0.firebaseapp.com",
    databaseURL: "https://tipbits-67cb0.firebaseio.com",
    projectId: "tipbits-67cb0",
    storageBucket: "tipbits-67cb0.appspot.com",
    messagingSenderId: "78548231892"
};
firebase.initializeApp(config);*/


export default class AddTipScreen extends Component {
  constructor(props) {
    super(props);
    this.database = firebase.database();
    this.state = {
      modalVisible: false,
      tags: [],
      user: null
    };
    this._addTip = this._addTip.bind(this);
    this._addToDatabase = this._addToDatabase.bind(this);
    this.state.content = "";
    this.extra = "";
    this.tags = "";
    this.key = "";
    this.firstInput;
    this.secondInput;
    this.thirdInput;
    this.CHAR_LIM = 250;
  }

  //calls addCategory from App.js and hides the addCategory popup
  async _addTip() {
    //formats the tags
    let tags = this.state.tags;
    let content = this.state.content.replace(/\s+/g, ' ');
    let errMessage;
    let success=false;
    // 
    if(tags.length === 0 || content.length<10) {
      errMessage = "Make sure to add content and at least one tag!";
    }
    if(this._hasDuplicates(this.state.tags)) {
      errMessage = "You can't have duplicate tags!";
    }
    if (content.length>this.CHAR_LIM) {
      errMessage = "Content must be less than " + this.CHAR_LIM + " characters. Use the description field for extra!";
    }
    if(errMessage) {
      Alert.alert(errMessage);
    }
    else {
      // Alert.alert("Adds to database;");
      // 
      // 
      success = this._addToDatabase(this.state.user, content, this.extra, tags);
      await success;
      //clears all text from the TextInput when a tip is submitted
    }
    if(success) {
      this.firstInput.clear();
      this.secondInput.clear();
      this.thirdInput.clear();
      this.setState({content: ""});
      this.extra = "";
      this.tags = "";
      this.key = "";
      this.props.navigation.navigate('Home', {}, NavigationActions.navigate({
        routeName: "HomeScreen",
        params: {addTip:true}
      }));
    }
    Amplitude.logEventWithProperties("Add Tip", {success: success.toString()});
  }

  _formatTags(text) {
    let tags = text.split(/\s+/g);
    if(tags.length > 0 && !(tags[tags.length-1]))
      tags.splice(tags.length-1, 1);
    this.setState({tags: tags});
  }

  _hasDuplicates(arr) {
    for(let i = 0; i<arr.length; i++) {
      if(arr.indexOf(arr[i]) !== i) return true;
    }
    return false;
  }

  //@desc Adds the tip to the firebase database
  //@param user the user object that created this tip
  //@param content the actual content of the tip
  //@param extra the extra description of the tip
  //@param tags the tags or categories of the tip formatted as an array of strings
  async _addToDatabase(user, content, extra, tags) {
    this.key = this.database.ref().child('tips').push().key;

    let updates = {};
    let tip = {
      categories: tags,
      confirms: 0,
      nopes: 0,
      content: content,
      userid: user.uid
    };

    updates['/tips/' + this.key] = tip;
    updates['/extras/' + this.key] = {extra: extra, userid: user.uid};
    updates['/users/'+user.uid+'/tips/' + this.key] = tip;
    for (let val of tags) {
      updates['/categories/' + val.toLowerCase() + '/' + this.key] = true;
    }
    let ret = true;
    await this.database.ref().update(updates)
    .catch((error) => {
      // 
      // 
      // 
      Alert.alert("Something went wrong.");
      ret = false;
    });
    return ret;
  }
  //subscriptions
  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        this.setState({user: user});
    });
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      Amplitude.logEvent("Add Tip Page");
    });
  }
  //unsusbscribe to subscriptions
  componentWillUnmount() {
    if(this.unsubscribe)
      this.unsubscribe();
    if(this.focusListener)
      this.focusListener.remove();
  }

  //renders a popup menu with the ability to add text input
  //Also the ability to close the popup menu
  render() {
    let charsRemaining = this.CHAR_LIM - this.state.content.length;

    return (
      <View style={{flex:1, backgroundColor: 'tomato', padding: 10, paddingTop: 30}}>
          <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps="handled">
              <TextInput
                onChangeText={(text) => this.setState({content:text})}
                placeholder="Your awesome tip goes here..."
                placeholderTextColor="#ededed"
                autoFocus={true}
                multiline={true}
                maxLength={this.CHAR_LIM}
                onSubmitEditing={()=> this.secondInput.focus()}
                style={styles.medInput}
                ref={(input) => {this.firstInput = input;}}
              />
              <View style={{width:'90%', flexDirection:'row', marginBottom:5}}><Text style={{color:'#ededed'}}>Chars remaining: { charsRemaining }</Text></View>
              <View style={{marginBottom: 5, width:'90%'}}>
                <TextInput
                  onChangeText={(text) => this._formatTags(text)}
                  placeholder="Add tags (separated by spaces) here..."
                  placeholderTextColor="#ededed"
                  style={{...styles.shortInput, marginBottom: 0}}
                  onSubmitEditing={()=> this.thirdInput.focus()}
                  ref={(input) => {this.secondInput = input;}}
                />
                <CategoryList tags={this.state.tags} pressable={false} />
              </View>
              <TextInput
                onChangeText={(text) => this.extra=text}
                placeholder="Add a (optional) description here..."
                placeholderTextColor="#ededed"
                style={styles.longInput}
                multiline={true}
                ref={(input) => {this.thirdInput = input;}}
              />
              <TouchableOpacity
                onPress={()=> {
                  this._addTip();
                }}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>SHARE THIS TIP!</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={()=> {
                  this.props.navigation.navigate('Home');
                }}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>CANCEL</Text>
                </View>
              </TouchableOpacity>
          </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop: 50,
    alignItems: "center"
  },
  shortInput: {
    borderColor: 'white',
    borderLeftWidth: 2,
    width: '90%',
    color: 'white',
    marginBottom: 5,
    paddingLeft: 7,
    fontSize: 18
  },
  tagsInput: {
    borderColor: 'white',
    borderLeftWidth: 2,
    fontSize: 18,
    paddingLeft: 7,
    color: 'white'
  },
  medInput: {
    borderColor: 'white',
    borderLeftWidth: 2,
    width: '90%',
    minHeight: 50,
    maxHeight: 100,
    color: 'white',
    alignItems: 'flex-start',
    paddingLeft: 7,
    textAlignVertical: 'top',
    fontSize: 18
  },
  longInput: {
    borderColor: 'white',
    borderLeftWidth: 2,
    width: '90%',
    height: 100,
    color: 'white',
    alignItems: 'flex-start',
    paddingLeft: 7,
    textAlignVertical: 'top',
    fontSize: 18
  },
  button: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop:15,
    borderRadius: 10,
    ...Platform.select({
      ios: {
      },
      android: {
        elevation: 4
      }
    })
  },
  buttonText: {
    color: 'tomato',
    fontSize: 20,
    fontWeight: 'bold'
  }
});
