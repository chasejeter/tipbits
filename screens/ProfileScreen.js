//@desc User profile screen where they can sign out, see posted tips, and see saved tips.

import React from 'react';
import { StyleSheet, Text, View, Alert, Button, FlatList } from 'react-native';
import Tip from '../components/Tip.js';
import Header from '../components/Header.js';
import TipObj from '../classes/TipObj.js';
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';

export default class ProfileScreen extends React.Component {
  //Is this necessary?
  static navigationOptions = {
    title: 'Profile',
  };

  constructor(props) {
    super(props);
    //@param posts when true, display a user's posts, otherwise, display a user's saved posts
    this.state = {
      tips: [],
      saved: [],
      user: null,
      userdata: null,
      posts: true
    };
    this._addTip = this._addTip.bind(this);
    this.unsubscribe = null;
    this.focusListener = null;
    //Unsubscribers for event listeners
    this.eUn = {
      tips: null,
      saved: null
    }
  }

  //Useless method
  _addTip(content, extra, _tags) {
    tips = this.state.tips;
    tips.push(new TipObj(content, _tags, 0, 0));
    this.setState({
      tips: tips
    });
  }

  componentDidMount() {
    //occurs when user navigates to this page
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      Amplitude.logEvent("Profile Page");
    })
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({user: user});
        //When a new tip of the current user is added to the database, add it to this.state.tips
        this.eUn.tips = firebase.database().ref('/users/' + user.uid +'/tips/').on('value', (snapshot) => {
          let tips = snapshot.val();
          let stateTips = [];
          for (let key in tips) {
            let obj = tips[key];
            stateTips.push(new TipObj(obj.content, obj.categories, obj.confirms, obj.nopes, key));
          }
          this.setState({tips: stateTips});
          // ...
        });
        //gets the user data
        firebase.database().ref('/users/' + user.uid).once('value').then((snapshot) => {
          this.setState({userdata: snapshot.val()});
        });
        //gets the saved tips of a user whenever it changes
        this.eUn.saved = firebase.database().ref('/users/' + user.uid +'/saved/').on('value', (snapshot) => {
          let saved = snapshot.val();
          let stateSaved = [];
          for (let key in saved) {
            let obj = saved[key];
            firebase.database().ref('/tips/').child(key).once('value', (snapshot) => {
              if(snapshot.val())
                stateSaved.push(new TipObj(obj.content, obj.categories, obj.confirms, obj.nopes, key));
              else
                firebase.database().ref('/users/' + user.uid + '/saved/' + key).remove();
            })
          }
          this.setState({saved: stateSaved});
          // ...
        });
      } else {
        // No user is signed in.
        // 
        Alert.alert("Something went wrong.");
      }
    });
  }

  componentWillUnmount() {
    if(this.unsubscribe)
      this.unsubscribe();
    firebase.database().ref('/users/' + this.state.user.uid +'/tips/').off('value', this.eUn.tips);
    firebase.database().ref('/users/' + this.state.user.uid +'/saved/').off('value', this.eUn.saved);
    if(this.focusListener)
      this.focusListener.remove();
  }

  changeDisplay() {
    this.setState((previousState) => ({posts: !previousState.posts}));
    //
  }

  render() {
    let uname = "";
    if (this.state.userdata) {
      uname = this.state.userdata.username;
    }
    //
    let buttonTitle = this.state.posts ? "Saved":"Posts";
    /*let tipList = <FlatList
        extraData={this.state}
        data={tipKeys}
        renderItem={({item}) => {
          return <Tip tags={this.state.tips[item.key].categories} content={this.state.tips[item.key].content} tipKey={this.state.tips[item.key].key} uid={this.state.user.uid}/>
        }}
      />;*/
    //An empty view that acts as a buffer between first tip and top of screen
    let margin = <View style={{width:"100%", height:10}} />;
    return (
      <View style={styles.mainContainer}>
        <Header title={uname} />
        <View style={styles.profileContainer}>
          <View style={{height:1, backgroundColor:'gray'}}/>
          <Button
            title="Signout"
            onPress={() => {
              firebase.auth().signOut().then(() => {
                Amplitude.setUserId(null);
                this.props.navigation.navigate('Auth');
              });
            }}
            color="tomato"
          />
          <View style={{height:1, backgroundColor:'gray'}}/>
          <Button
            title={buttonTitle}
            onPress={() => {
              if(buttonTitle == "Saved")
                Amplitude.logEvent("Saved Page");
              this.changeDisplay();
            }}
            color="tomato"
          />
        </View>
        <FlatList
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={margin}
            extraData={this.state}
            data={this.state.tips}
            contentContainerStyle={{display: this.state.posts ? "default" : "none"}}
            renderItem={({item}) => {
              return (
                <View style={{width:"90%", alignSelf:'center'}}>
                  <Tip tags={item.categories} content={item.content} tipKey={item.key} uid={this.state.user.uid} deletable={true}/>
                </View>
              )
            }}
        />
        <FlatList
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={margin}
            extraData={this.state}
            data={this.state.saved}
            contentContainerStyle={{display: !this.state.posts ? "default" : "none"}}
            renderItem={({item}) => {
              return (
                <View style={{width:"90%", alignSelf:'center'}}>
                  <Tip tags={item.categories} content={item.content} tipKey={item.key} uid={this.state.user.uid}/>
                </View>
              );
            }}
        />
      </View>
    )
  }
}
/*


*/
const styles = StyleSheet.create({
  mainContainer: {
    flex:1,
    backgroundColor: 'tomato'
  },
  profileContainer: {
    // width: '100%',
    backgroundColor:'white',
    borderColor: 'gray',
    borderBottomWidth: 1,
  }
});
