//Loading screen that redirects to home screen or auth screen
import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableHighlight, Alert, Modal } from 'react-native';
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';


export default class AuthLoadingScreen extends React.Component {


  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        if(user) {
          Amplitude.setUserId(user.uid);
          //this.props.navigation.navigate('App');
          //the tutorial screen
          this.props.navigation.navigate('Tutorial');
        }
        else {
          this.props.navigation.navigate('Auth');
        }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }


  render() {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:22.5,
    flex: 1,
    backgroundColor: 'tomato',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
