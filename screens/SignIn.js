//Page for user to sign into their account
import React from 'react';
import { StyleSheet, Text, View, TextInput, Alert, Modal, Platform, TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack'
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';


class SignIn extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return ({
      title: navigation.getParam('title', 'Sign Up for Tipbits')
    })
  }

  state = {
    newUser : true,
    modalVisible: false
  }
  login = {
    email: "",
    pass: "",
    user: "",
    name: ""
  }

  componentDidMount() {
    Amplitude.logEvent("Welcome");
  }

  /*
    @desc creates a new push notification token and adds it to database
    @param uid user id that is used to reference user in database
  */
  newPushToken(uid) {

  }

  setModalVisible(tof) {
    this.setState({modalVisible: tof});
  }

  verifyData() {
    let errMessage;
    if(this.login.user.length < 6)
      errMessage = "Username must have at least 5 characters";
    else if(/\s/g.test(this.login.user))
      errMessage = "Username must not have whitespace";
    else if(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(this.login.name))
      errMessage = "Your name must not have special characters";
    else if(this.login.name.length < 3)
      errMessage = "Your name must at least have 2 characters"
    else {
      //database refs are case sensitive
      errMessage = firebase.database().ref('usernames/' + this.login.user.toLowerCase()).once('value').then((snap) => {
        let val = snap.val();
        if(val)
          return "Username already exists";
        else {
          return true;
        }
      });
    }
    if(errMessage)
      return errMessage
    else {
      return true;
    }
  }

  //test function
  async createNewUserTest() {
    this.login = {
      user: "Chasejete",
      name: "Chase"
    };
    let message = this.verifyData();

  }

  //Creates a new user
  async createNewUser() {
    let user = null;
    let message = await this.verifyData();
    if(message !== true) {
      Alert.alert(message);
      return false;
    }

    firebase.auth().createUserWithEmailAndPassword(this.login.email, this.login.pass)
    .then((userData) => {
      user = userData.user;
      firebase.database().ref('users/' + user.uid).set({
        username: this.login.user,
        email: this.login.email,
        name: this.login.name,
      });
      firebase.database().ref('usernames/' + this.login.user.toLowerCase()).set(user.uid);
      return user.uid;
    })
    .then((id) => {
      //navigate to the tutorial screen
      Amplitude.setUserId(id);
      //Add property to tell if sign up was successful?
      Amplitude.logEvent("Sign up");
      this.props.navigation.navigate('Tutorial');
    })
    .catch(function(error) {
      // Handle Errors here.
      //
      if (user) {
        //If there are any errors then delete the new user
        user.delete().
        catch((err) => {
          //
          //
          Alert.alert("Something went wrong.");
        });
      }
      var errorCode = error.code;
      var errorMessage = error.message;
      //
      Alert.alert(errorMessage);
      // ...
    });
  }

  //signs a user in with current email and password
  async signIn() {
    firebase.auth().signInWithEmailAndPassword(this.login.email, this.login.pass)
    .then((userData) => {
      //Add property to tell if sign in was successful?
      Amplitude.logEvent("Sign in");
      Amplitude.setUserId(userData.user.uid);
      this.props.navigation.navigate('App');
    })
    .catch(function(error) {
      // Handle Errors here.
      // NEED TO MAKE A REAL ERROR HANDLER
      var errorCode = error.code;
      var errorMessage = error.message;
      Alert.alert(errorMessage);
      // ...
    });
  }

  render() {
    let meth;
    let text = "";
    let newUserMarkup = [];
    let forgotPassword;
    //if on the sign in option, use signIn(). Otherwise use createNewUser()
    if (this.state.newUser) {
      meth = () => {
        this.createNewUser();
      }
      newUserMarkup.push(
        <TextInput
          placeholder="Username"
          style={styles.shortInput}
          placeholderTextColor="#ededed"
          onChangeText={(text) => {
              this.login.user = text;
            }}
        />
      );
      newUserMarkup.push(
        <TextInput
          placeholder="Your name"
          style={styles.shortInput}
          placeholderTextColor="#ededed"
          onChangeText={(text) => {
              this.login.name = text;
            }}
        />
      );
      submitText = "Sign Up";
      orText = "Sign In";
    }
    else {
      meth = () => {
        this.signIn();
      }
      forgotPassword = (
        <TouchableOpacity
          onPress={()=> {
            this.props.navigation.navigate("ForgotPass");
          }}>
          <View style={styles.sButton}>
            <Text style={styles.sButtonText}>Forgot password?</Text>
          </View>
        </TouchableOpacity>
      );
      submitText = "Sign In";
      orText = "Sign Up";
    }
    return (
            <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps="handled">
              <TextInput
              placeholder="Email"
              style={styles.shortInput}
              placeholderTextColor="#ededed"
              onChangeText={(text) => {
                  this.login.email = text;
              }}/>

              {newUserMarkup[0]}
              {newUserMarkup[1]}

              <TextInput
              placeholder="Password"
              secureTextEntry={true}
              style={styles.shortInput}
              placeholderTextColor="#ededed"
              onChangeText={(text) => {
                  this.login.pass = text;
                }
              }
              />

              <TouchableOpacity
                onPress={meth}>
                <View style={styles.bButton}>
                  <Text style={styles.bButtonText}>{submitText}</Text>
                </View>
              </TouchableOpacity>

              { forgotPassword }

              <Text style={{color: 'white', marginTop:10}}>or</Text>
              <TouchableOpacity
                onPress={()=> {
                  this.setState((previousState) => {
                    this.props.navigation.setParams({
                      title: (!previousState.newUser) ? "Sign Up for Tipbits":"Sign In to Tipbits",
                    });
                    return ({newUser: !previousState.newUser})
                  });
                }}>
                <View style={styles.sButton}>
                  <Text style={styles.sButtonText}>{orText}</Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
    );
  }
}

class ForgotPassModal extends React.Component {
  static navigationOptions = {
    title: "Forgot Password"
  }
  email = "";
  sendRecoveryEmail() {
    firebase.auth().sendPasswordResetEmail(this.email).then(() => {
      Alert.alert("Instructions to reset password were sent to your email.");
      this.props.navigation.goBack();
    }).catch(function(err) {
      if(err.code === "auth/invalid-email") {
        Alert.alert("Invalid email address. Check your email format.")
      }
      else if(err.code === "auth/user-not-found") {
        Alert.alert("No user exists with this email address.");
      }
      else {
        Alert.alert("Something went wrong...Try again.");

      }
    });

  }
  render() {

    return (
      <ScrollView contentContainerStyle={styles.container} keyboardShouldPersistTaps="handled">
        <TextInput
          placeholder="Your email"
          style={styles.shortInput}
          placeholderTextColor="#ededed"
          onChangeText={(text) => {
              this.email = text;
          }}/>
        <TouchableOpacity
          onPress={() => {this.sendRecoveryEmail()}}>
          <View style={styles.sButton}>
            <Text style={styles.sButtonText}>Reset password</Text>
          </View>
        </TouchableOpacity>
      </ScrollView>
    )
  }
}

export default createStackNavigator({
  SignIn: {
    screen: SignIn
  },
  ForgotPass: {
    screen: ForgotPassModal
  }
}, {mode: 'modal'});

const styles = StyleSheet.create({
  container: {
//    paddingTop:22.5,
    flex: 1,
    backgroundColor: 'tomato',
    alignItems: 'center',
    justifyContent: 'center',
  },
  shortInput: {
    borderColor: 'white',
    borderBottomWidth: 1,
    color: 'white',
    marginBottom: 5,
    fontSize: 18,
    minWidth: 100,
    textAlign: "center"
  },
  bButton: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop:10,
    borderRadius: 10,
    ...Platform.select({
      ios: {
      },
      android: {
        elevation: 4
      }
    })
  },
  bButtonText: {
    color: 'tomato',
    fontSize: 20,
    fontWeight: 'bold'
  },
  sButton: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    marginTop:10,
    borderRadius: 10,
    ...Platform.select({
      ios: {
      },
      android: {
        elevation: 4
      }
    })
  },
  sButtonText: {
    color: 'tomato',
    fontSize: 15,
    fontWeight: 'bold'
  }
});
