//@desc A screen for displaying only tips in a specific category. Exists within the Home stack.

import React from 'react';
import { StyleSheet, Text, View, Alert, FlatList, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Tip from '../../components/Tip.js';
import TipObj from '../../classes/TipObj.js';
import * as firebase from 'firebase';

export default class CategoryScreen extends React.Component {
  //@prop name The name of the category
  static navigationOptions = ({ navigation }) => {
    const params = navigation.state.params || {name: 'error'};

    return {
      title: params.name,
      headerTitleStyle: {fontWeight:'bold', fontSize:30},
      headerStyle: {height:50},
      headerRight: () => (
        <TouchableOpacity
          onPress={() => {
            let addCategory = navigation.getParam("addCategory");
            if(addCategory)
              addCategory(navigation);
          }}
          style={{marginRight:10}}
        >
          {/*<Text style={{fontSize:100}}>Hello</Text>*/}
          <Ionicons name={navigation.getParam("categoryAdded") ? "ios-checkmark" : "ios-add"} color="#2aa7f5" size={40} />
        </TouchableOpacity>
      )
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      tips: [],
      user: null,
      featured: [],
    };
    this.unsubscribe = null;
    this.unsubscribeTips = null;
    this.params = this.props.navigation.state.params;
    if(this.params == null || this.params.name == null)
      this.params.name = 'error';
  }

  //Subscriptions
  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      this.setState({user: user});
      firebase.database().ref('/userCategories/' + user.uid + '/' + this.params.name).once('value')
      .then((snap) => {
        this.props.navigation.setParams({categoryAdded: snap.val()});
      });
    });
    //when a new tip is added, add the tip to the homescreen
    this.unsubscribeTips = firebase.database().ref('/categories/' + this.params.name.toLowerCase()).on('value', (snapshot) => {
      //
      //let tipKeys = snapshot.val();
      let stateTips = [];
      let prom;
      snapshot.forEach((childData) => {
        let key = childData.key;
        if(key != 'count') {
          prom = firebase.database().ref('/tips/' + key).once('value')
          .then((snapshot) => {
            let obj = snapshot.val();
            stateTips.push(new TipObj(obj.content, obj.categories, obj.confirms, obj.nopes, key));
          })
          .catch((err) => {
            //
            Alert.alert("Something went wrong.");
          });
        }
      });
      prom.then(() => {
        this.setState({tips: stateTips});
      });
      // ...
    });
    this.props.navigation.setParams({addCategory: this._addCategory.bind(this)});

  }

  //unsubscribes stuff here
  componentWillUnmount() {
    if(this.unsubscribe)
      this.unsubscribe();
    if(this.unsubscribeTips)
      firebase.database().ref('/categories/' + this.params.name.toLowerCase()).off('value', this.unsubscribeTips);
  }

  _addCategory() {
    let current = this.props.navigation.getParam("categoryAdded") || false;
    firebase.database().ref('/userCategories/' + this.state.user.uid + '/' + this.params.name).set((!current || null))
    .then(() => {
      this.props.navigation.setParams({categoryAdded: !current});
    });
  }

  render() {
      //An empty view that acts as a buffer between first tip and top of screen
    let header = <View style={{marginTop: 10}} />;

    //
    return (
          <View style={{flex: 1, backgroundColor:'tomato'}}>
            <FlatList
              showsVerticalScrollIndicator={false}
              extraData={this.state}
              ListHeaderComponent={header}
              data={this.state.tips}
              renderItem={({item}) => {
                return (
                  <View style={{width:"90%", alignSelf:'center'}}>
                    <Tip tags={item.categories} content={item.content} tipKey={item.key} uid={this.state.user.uid}/>
                  </View>
                )
              }}
            />
          </View>
    );
  }
}

const styles = StyleSheet.create({
  category: {
    backgroundColor: 'gray',
    color: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderRadius: 15,
    marginLeft: 3
  },
  header: {
    width: "100%",
    backgroundColor: 'white',
    paddingTop: 32.5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    marginBottom: 10,
    marginTop: -20
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});
