//Home screen of app that displays popular tips
import React from 'react';
import { Platform, Vibration, StyleSheet, Text, View, TextInput, TouchableHighlight, SafeAreaView, Alert, Button, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { Notifications } from 'expo';
import { Entypo } from '@expo/vector-icons';
import * as Permissions from 'expo-permissions';
import {NavigationActions} from 'react-navigation';
import Tip from '../../components/Tip.js';
import Category from '../../components/Category.js';
import TipObj from '../../classes/TipObj.js';
import * as firebase from 'firebase';
require('firebase/functions');
import * as Amplitude from 'expo-analytics-amplitude';

let count = 1;

// class Header extends React.Component {
//   render() {
//       count++;
//       //An empty view that acts as a buffer between first tip and top of screen
//       return(
//         <View>
//           <Text style={{marginTop: 20, fontWeight:'bold', fontSize:30}}>Tipbits</Text>
//           <View style={{flexDirection:'row'}}>
//             <Text style={{fontSize:20}}>Featured: </Text>
//             <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
//               { this.props.tagsDisplay }
//             </ScrollView>
//           </View>
//         </View>
//       );
//   }
// }

// class Header extends React.Component {
//   render() {
//       count++;
//       //An empty view that acts as a buffer between first tip and top of screen
//       return(
//
//       );
//   }
// }

export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
      title: 'Tipbits',
      headerStyle: {height:50},
      headerTitleStyle: {fontWeight:'bold', fontSize:30},
      headerLeft: () => (
        <TouchableOpacity
          onPress={() => {
            let toggleModal = navigation.getParam('toggleModal');
            if(toggleModal) toggleModal();
          }}
          style={{marginLeft:5}}
        >
          <Entypo name="grid" size={32} color="black" />
        </TouchableOpacity>
      )
  });

  constructor(props) {
    super(props);
    this.state = {
      tips: [],
      user: null,
      featured: [],
      userTags: [],
      modalVisible: false
    };
    this.unsubscribe = null;
    this.unsubscribeTips = null;
    this.focusListener = null;
    this.flatListRef = null;
    this._findHeaderTags().then((headerTags) => {
      this.props.navigation.setParams({
        headerTags: headerTags
      });
      this.headerTags = headerTags;
    })
    this.firstMount = true;
  }
  //Finds the top 5 categories
  async _findHeaderTags() {
    let retTags;
    await firebase.database().ref('/categories').orderByChild('count').limitToLast(5).once('value')
      .then((snapshot) => {
        let tagsDisplay = [];
        snapshot.forEach(function(childSnapshot) {
          let tag = childSnapshot.key;
          tagsDisplay.unshift(<Category name={tag} key={tag} />);
        });
        retTags = tagsDisplay;
      })
      .catch((err) => {
        throw err;
      });
    return retTags;
  }
  // registers for a push notification
  registerForPushNotificationsAsync = async (uid) => {

    // gets current status of notification permissions
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;

    // if app does not have permissions, ask for them
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if(finalStatus !== 'granted') {
      Alert.alert("Please turn on your notifications.");
      Amplitude.setUserProperties({notifications_enabled:"false"});
      return;
    }
    Amplitude.setUserProperties({notifications_enabled:"true"});
    let token = await Notifications.getExpoPushTokenAsync();
    firebase.database().ref('users/'+uid +'/expoToken').set(token);
    this.expoPushToken = token;

    if (Platform.OS === 'android') {
      Notifications.createChannelAndroidAsync('default', {
        name: 'default',
        sound: true,
        priority: 'max',
        vibrate: [0, 250, 250, 250],
      });
    }
  };

  //Subscriptions
  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        this.setState({user: user});
        this.registerForPushNotificationsAsync(user.uid);
        this._getUserTags(user.uid);
    });
    //when a new tip is added, add the tip to the homescreen. Sorts tips received by number of confirmations.
    // this.unsubscribeTips = firebase.database().ref('/tips/').orderByChild('confirms').on('value', (snapshot) => {
    //   // let tips = snapshot.val();
    //   let stateTips = [];
    //   snapshot.forEach(function(childSnapshot) {
    //     let obj = childSnapshot.val();
    //     let key = childSnapshot.key;
    //     stateTips.unshift(new TipObj(obj.content, obj.categories, obj.confirms, obj.nopes, key));
    //   });
    //   this.tips = stateTips;
    //   if(this.firstMount) {
    //     this.forceUpdate();
    //     this.firstMount = false;
    //   }



      // ...
    //});

    this._getHome();

    // subscribes to followed tags


    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      // if(this.props.navigation.getParam('addTip', false)) {
      //   this.forceUpdate();
      //   this.flatListRef.scrollToEnd();
      //   this.props.navigation.setParams({addTip: false});
      // }
      //
      Amplitude.logEvent("Home");
    });
    this._notificationSubscription = Notifications.addListener(this._handleNotification);
    this.props.navigation.setParams({toggleModal: this._toggleModal.bind(this)});
  }

  _getHome() {
    let getHomePage = firebase.functions().httpsCallable('getHomePage')
    getHomePage().then((tips) => {
      if(tips === false){
        Alert.alert("Something went wrong.");
        return;
      }
      this.setState({tips: tips.data});
    });
  }

  _getUserTags(uid) {
    this.unsubscribeTags = firebase.database().ref('/userCategories/' + uid).on('value', (snap) => {
      if(snap.val()) {
        let tags = []
        snap.forEach((child) => {
          let val = child.val();
          if(val) tags.push(child.key);
        });
        this.setState({userTags: tags});
      }
    });
  }

  // handles the notification
  _handleNotification = notification => {
    if(Platform.OS === 'android')
      Notifications.dismissAllNotificationsAsync();
    if(notification.origin === 'selected') {
      const navigateAction = NavigationActions.navigate({
        routeName: 'App',
        action: NavigationActions.navigate({ routeName: 'Notification' }),
      });
      this.props.navigation.dispatch(navigateAction);
    }
    else {
      if(notification.remote) {
        Vibration.vibrate();
        const notificationId = Notifications.presentLocalNotificationAsync({
          title: notification.data.message,
          body: notification.data.message,
          ios: { _displayInForeground: true }
        });
      }
      else {
        const navigateAction = NavigationActions.navigate({
          routeName: 'App',
          action: NavigationActions.navigate({ routeName: 'Notification' }),
        });
        this.props.navigation.dispatch(navigateAction);
      }
    }
  };

  //unsubscribes stuff here
  componentWillUnmount() {
    if(this.unsubscribe)
      this.unsubscribe();
    if(this.unsubscribeTips)
      firebase.database().ref('/tips/').off('value', this.unsubscribeTips);
    if(this.focusListener)
      this.focusListener.remove();
    if(this._notificationSubscription)
      this._notificationSubscription.remove()
    if(this.unsubscribeTags) firebase.database().ref('/userTags/' + this.state.user.uid).off('value', this.unsubscribeTags);
  }

  _toggleModal() {
    this.setState((previousState) => ({
      modalVisible: !previousState.modalVisible
    }))
  }

  _navigateToTag(tag) {
    this._toggleModal();
    this.props.navigation.navigate('Category', {name: tag});
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <MenuModal isVisible={this.state.modalVisible} toggle={this._toggleModal.bind(this)} navigateToTag={this._navigateToTag.bind(this)} items={this.state.userTags} />
        <View style={{flexDirection:'row', backgroundColor:'white', alignItems:'center', paddingVertical:5, paddingLeft:5}}>
          <Text style={{fontSize:20}}>Featured: </Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            { this.headerTags }
          </ScrollView>
        </View>
        <FlatList
          ref={(ref) => {this.flatListRef = ref;}}
          onRefresh={() => {this._getHome();}}
          refreshing={false}
          showsVerticalScrollIndicator={false}
          extraData={this.state}
          ListHeaderComponent={<View style={{marginTop:10}} />}
          data={this.state.tips}
          renderItem={({item}) => {
            return (
              <View style={{width:"90%", alignSelf:'center'}}>
                <Tip tags={item.categories} content={item.content} tipKey={item.key} uid={this.state.user.uid}/>
              </View>
            );
          }}
        />
      </View>
    )
  }
}

class MenuModal extends React.Component {

  render() {
    let display = [];
    if(this.props.items)
      display = this.props.items.map((item) => <TouchableOpacity onPress={() => this.props.navigateToTag(item)} key={item} style={{paddingVertical:10, borderBottomWidth:1, borderColor:'gray'}}><Text style={{alignSelf: 'center', fontSize:24, color:'tomato'}}>{item}</Text></TouchableOpacity>);
    return (
        <Modal
          animationIn="slideInLeft"
          animationOut="slideOutLeft"
          isVisible={this.props.isVisible}
          statusBarTranslucent={true}
          style={{margin:0}}
        >
          <SafeAreaView style={{flex: 1, flexDirection:'column', justifyContent:'flex-start', alignItems:'stretch', paddingTop:5, backgroundColor: 'white'}}>

            <View style={{flexDirection:'row', justifyContent:'space-between', height:60, alignItems:'center', borderBottomWidth:1, borderColor:'gray'}}>
              <TouchableOpacity style={{marginLeft:5}} onPress={this.props.toggle}>
                <Entypo name="cross" size={36} color="black" />
              </TouchableOpacity>
              <Text style={{fontSize: 32}}>Your tags</Text>
              <View style={{backgroundColor:'white', height:25, width:25, marginRight:5}}></View>
            </View>
            { display }
          </SafeAreaView>
        </Modal>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex:1,
    backgroundColor: 'tomato'
  },
  header: {
    marginTop: 5,
    borderWidth: 0
  },
  category: {
    backgroundColor: 'gray',
    color: 'white',
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 2,
    borderRadius: 15,
    marginLeft: 3
  }
});
