//Home screen of app that displays popular tips
import React from 'react';
import { StyleSheet, ScrollView, SafeAreaView, Text, View, TextInput, TouchableHighlight, Alert, Button, FlatList, Platform, TouchableOpacity } from 'react-native';
import BaseTip from '../components/BaseTip.js';
import Tip from '../components/Tip.js';
import TipObj from '../classes/TipObj.js';
import { Ionicons } from '@expo/vector-icons';
import * as firebase from 'firebase';
import TutorialContext from '../contexts/TutorialContext.js';
import * as Amplitude from 'expo-analytics-amplitude';

export default class Tutorial extends React.Component {

  constructor(props) {
    super(props);
    this.unsubscribe = null;
    this.unsubscribeTips = null;
  }

  componentDidMount() {
    Amplitude.logEvent("Tutorial");
  }

  render() {
    //
    //An empty view that acts as a buffer between first tip and top of screen
    let margin = <View style={{width:"100%", height:30}} />;
    let content = "This is a tip!";
    let categories = ["Tutorial", "Tipbits"];
    let extra = "This is where extra explanation or further description goes!";

    return (
      <SafeAreaView style={styles.mainContainer}>
        <ScrollView contentContainerStyle={{width: '90%', alignSelf:'center', paddingTop:40, paddingBottom: 40}}>
          <TutorialContext.Provider value={{notActive: true}}>
            <BaseTip tags={categories} content={content} extra={extra} notActive={true}/>
          </TutorialContext.Provider>
          <Text style={{color:'white', fontSize:27}}>- Use the "
          <Ionicons name={'ios-arrow-dropup-circle'} size={35} color={'white'} />" button to agree with a tip. Try it! </Text>
          <Text style={{color:'white', fontSize:27}}>- Use the "
          <Ionicons name={'ios-arrow-dropdown-circle'} size={35} color={'white'} />" button to disagree with a tip. Try it! </Text>
          <Text style={{color:'white', fontSize:27}}>- Use the "
          <Ionicons name={'ios-add-circle'} size={35} color={'white'} />" button to save a tip for later. You can view your saved tips under 'profile'. Try it! </Text>
          <Text style={{color:'white', fontSize:27}}>- Press the tip to view more information. Try it!</Text>
          <TouchableOpacity
            onPress={()=> {
              this.props.navigation.navigate('App');
            }}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>START SHARING!</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>

    );
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'tomato'
  },
  button: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop:15,
    borderRadius: 10,
    ...Platform.select({
      ios: {
      },
      android: {
        elevation: 4
      }
    })
  },
  buttonText: {
    color: 'tomato',
    fontSize: 20,
    fontWeight: 'bold'
  }
});
