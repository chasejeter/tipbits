//Home screen of app that displays popular tips
import React from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, SafeAreaView, Alert, FlatList, ScrollView, Modal } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Header from '../components/Header.js';
import Category from '../components/Category.js';
import TipContainer from '../components/TipContainer.js';
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';

export default class NotificationScreen extends React.Component {
  static navigationOptions =  {
      title: 'Notifications'
  };

  state = {
    notifs: null
  }

  //Subscriptions
  componentDidMount() {
    //occurs when user navigates to this page
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      Amplitude.logEvent("Notification Page");
    })
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({uid: user.uid});
        this.setNotifsState(user.uid);
      }
      else {
        Alert.alert("Please sign in.");
      }
    });

  }

  setNotifsState = (uid) => {
    const notifsRef = firebase.database().ref("notificationsDisplay/"+uid);
    notifsRef.orderByChild('timestamp').on('child_added', (snap) => {
      let notifs = this.state.notifs ? this.state.notifs : [];
      let notifObj = snap.val();
      notifObj.key = snap.key;
      notifs.unshift(notifObj);
      this.setState({notifs: notifs});
    })
  }

  formatNotification(notif) {
    let content = "";
    let subcontent = notif.content;
    /*
      Notification format:
      Content: <NAME>[, NAME, and NAME...] < "agreed with" | "disagreed with" | "saved" > your tip!
      Subcontent: <CONTENT_SNIPPET>
    */
    if(notif.names.length === 1)
      content+= notif.names[0] + " ";
    else if(notif.names.length === 2)
      content += notif.names[0] + " and " + notif.names[1] + " ";
    else {
      let i;
      for(i = 0; i<notif.names.length-1; i++) {
        content += notif.names[i] + ", ";
      }
      let diff = notif.count - notif.names.length;
      if(diff === 0)
        content+="and " + notif.names[i] + " ";
      else
        content += notif.names[i] + ", and " + diff + " others ";
    }
    content += notif.action + "d ";
    if(notif.action === 'agree' || notif.action === 'disagree')
      content += 'with ';
    content += 'your tip!';
    return {content: content, subcontent: subcontent}
  }

  //unsubscribes stuff here
  componentWillUnmount() {
    if(this.unsubscribe)
      this.unsubscribe();
    firebase.database().ref("notificationsDisplay/"+this.state.uid).off();
    if(this.focusListener)
      this.focusListener.remove();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'tomato'}}>
        <Header title='Notifications' style={{borderBottomWidth:1, borderColor:'tomato'}}/>
        <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.notifs}
            renderItem={({item}) => {
              let notifObj = this.formatNotification(item);
              return <Notification content={notifObj.content} subcontent={notifObj.subcontent} tipKey={item.tipKey} />;
            }}
        />
      </View>
    )
  }
}

class Notification extends React.Component {
  loaded = false;
  state = {
    modalVisible: false,
    content: "",
    confirms: 0,
    nopes: 0,
    userid: "",
    extra: "",
    categories: []
  }
  modalVisible(tof) {
    this.setState({modalVisible: tof});
    if(!this.loaded) {
      this.loaded = true;
      this.loadTip();
    }
  }

  loadTip() {
    const tipRef = firebase.database().ref('tips/' + this.props.tipKey);
    const extraRef = firebase.database().ref('extras/' + this.props.tipKey + '/extra');
    let data;
    tipRef.once('value', (snap) => {
      // if tip does not exist
      if(!snap.exists()) {
        Alert.alert("Tip no longer exists.");
        this.modalVisible(false);
        return;
      }
      data = snap.val();
      extraRef.once('value', (extraSnap) => {
        data.extra = extraSnap.val();
        this.setState(data);
      });
    });
  }

  render() {
    //Changes the color of the icon depending on if the buttons have been toggled
    let confirmedColor = '#45cc60';
    let nopedColor = 'gray';
    let savedColor = 'gray';
    let tagsArray = [];
    for(let i = 0; i<this.state.categories.length; i++) {
        tagsArray.push(<Category name={this.state.categories[i]} key={this.state.categories[i]} />);
    }

    let tagsDisplay =
      (<View style={styles.tagsDisplay}>
        <Text>Tags: </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          { tagsArray }
        </ScrollView>
      </View>);

    let extra = {
      fontSize: 20,
      color: "#575757",
      marginBottom: 5,
    };
    if(this.state.modalVisible === false || this.state.extra.length === 0)
      extra.display = "none";
    return (
      <View>
        <TouchableWithoutFeedback
          onPress={()=> {
            this.modalVisible(true);
          }}
        >
          <View style={{padding:7, backgroundColor:'white', marginBottom:1, marginTop:4, marginHorizontal:5, borderColor:'tomato', borderRadius:5}}>
            <Text style={{fontSize:18, fontWeight:'bold'}} >{this.props.content}</Text>
            <Text style={{marginLeft:5, color:'gray'}}>{this.props.subcontent}...</Text>
          </View>
        </TouchableWithoutFeedback>

        <Modal
          animationType={'fade'}
          onRequestClose={() => this.modalVisible(false)}
          visible={this.state.modalVisible}
        >
          <SafeAreaView style={{flex: 1, backgroundColor:'tomato'}}>
            <TouchableWithoutFeedback
              onPress={()=> this.modalVisible(false)}>
              <View style={{flexDirection:'row-reverse', alignItems:'center'}}>
                <View style={{flex:.95, flexDirection:"row", alignItems:'center'}}>
                  <Ionicons name={'ios-arrow-back'} size={30} color={'white'} />
                  <Text style={{color:'white', fontSize:30, marginBottom:2}}> Back</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
            <ScrollView contentContainerStyle={{alignItems: 'center'}}>
              <View style={{width:"90%"}}>
                <TipContainer
                  content={this.state.content}
                  confirmed={true}
                  tags={this.state.categories}
                  extra={this.state.extra}
                  confirms={this.state.confirms}
                  nopes={this.state.nopes}
                  displayExtra={true}
                  confirm={()=>true}
                  nope={()=>true}
                  save={()=>true}
                />
              </View>
            </ScrollView>
            {/*<View style={styles.container}>
              <View style={{marginBottom: 10, backgroundColor:'white', padding: 10, flex: .9}}>
                <Text style={styles.text}>{this.state.content}</Text>
                <Text style={extra}>{this.state.extra}</Text>
                {
                  tagsDisplay
                }
                <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                  <View style={styles.button}>
                    <TouchableWithoutFeedback
                      onPress={() => {this.confirm()} }>
                      <Ionicons name={'ios-arrow-dropup-circle'} style={{marginRight: 5}} size={35} color={confirmedColor} />
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                      onPress={() => {this.nope()}}>
                      <Ionicons name={'ios-arrow-dropdown-circle'} size={35} color={nopedColor} />
                    </TouchableWithoutFeedback>
                    <Text style={{marginLeft: 7}}>{this.state.confirms-this.state.nopes}</Text>
                  </View>
                  <View style={styles.button}>
                    <Ionicons name={'ios-add-circle'} size={35} color={savedColor} />
                  </View>
                </View>
              </View>
            </View>*/}
          </SafeAreaView>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontSize: 24,
    fontWeight: "bold"
  },
  tagsDisplay: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 3
  },
  container: {
    flexDirection: "row",
    justifyContent:"center"
  }
});
