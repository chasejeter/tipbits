import React from 'react';
import { View, Text, SafeAreaView, TextInput, StyleSheet } from 'react-native';
import { Ionicons, FontAwesome5 } from '@expo/vector-icons';
import Header from '../components/Header.js';
import algoliasearch from 'algoliasearch/dist/algoliasearch.umd.js';
import { InstantSearch } from 'react-instantsearch-native';
import { connectSearchBox } from 'react-instantsearch-native';
import PropTypes from 'prop-types';
import InfiniteTips from '../components/InfiniteTips.js';
import * as firebase from 'firebase';
import * as Amplitude from 'expo-analytics-amplitude';

const searchClient = algoliasearch('UR3PUSHQUS', 'fd5add975c78aa129ffa11e280e572e4');

export default class ExploreScreen extends React.Component {

  state = {
    user: {
      uid: null
    }
  }

  componentDidMount() {
    //occurs when user navigates to this page
    this.focusListener = this.props.navigation.addListener('didFocus', () => {
      Amplitude.logEvent("Explore Page");
    })
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        this.setState({user: user});
    });
  }

  componentWillUnmount() {
    if(this.unsubscribe)
      this.unsubscribe();
    if(this.focusListener)
      this.focusListener.remove();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'tomato'}}>
        <InstantSearch
          searchClient={searchClient}
          indexName="tips"
          root={this.root}
        >
          <SearchBox />
          <InfiniteTips uid={this.state.user.uid} />
        </InstantSearch>
      </View>
    )
  }
}

const searchBox = ({ currentRefinement, refine }) => (
  <SafeAreaView style={{backgroundColor:'white'}}>
    <View style={styles.searchContainer}>
      <Ionicons name="md-search" size={32} color="#a3a3a3" />

      <TextInput
        onChangeText={value => refine(value)}
        value={currentRefinement}
        placeholder="Search..."
        placeholderTextColor="#a3a3a3"
        autoFocus={true}
        // onSubmitEditing={()=> this.secondInput.focus()}
        style={styles.shortInput}
        // ref={(input) => {this.firstInput = input;}}
      />
    </View>
    <Text style={{alignSelf: "center", marginBottom: 5, color: "gray"}}>Powered by <FontAwesome5 name="algolia" size={20} color="#5468ff" /> algolia...</Text>
  </SafeAreaView>
);

searchBox.propTypes = {
  currentRefinement: PropTypes.string.isRequired,
  refine: PropTypes.func.isRequired,
};

const SearchBox = connectSearchBox(searchBox);

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: "row",
    width: "90%",
    alignSelf:"center",
    backgroundColor: "#e8e8e8",
    marginBottom: 5,
    borderRadius:8,
    paddingHorizontal: 5,
    paddingTop:3
  },
  shortInput: {
    width: "90%",
    color: '#535353',
    paddingLeft: 7,
    fontSize: 24
  }
})
