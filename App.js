//main app where all of the navigation stuff is set up

import React from 'react';
import { Vibration, Platform, Alert } from 'react-native';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import HomeScreen from './screens/HomeStack/HomeScreen.js';
// import CategoryScreen from './screens/HomeStack/CategoryScreen.js';
import CategoryScreen from './screens/HomeStack/CategoryScreen.js';
import SignInStack from './screens/SignIn.js';
import Tutorial from './screens/Tutorial.js';
import ProfileScreen from './screens/ProfileScreen.js';
import AddTipScreen from './screens/AddTipScreen.js';
import AuthLoadingScreen from './screens/AuthLoading.js';
import NotificationScreen from './screens/NotificationScreen.js';
import ExploreScreen from './screens/ExploreScreen.js';
import { Ionicons } from '@expo/vector-icons';
import {createAppContainer, createSwitchNavigator, NavigationActions} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import { FIREBASE_API, FIREBASE_AUTH_DOMAIN, FIREBASE_DATABASE_URL, FIREBASE_PROJECT_ID, FIREBASE_STORAGE_BUCKET, FIREBASE_MESSAGING_SENDER_ID, FIREBASE_APP_ID, FIREBASE_MEASUREMENT_ID, AMPLITUDE_API } from '@env';
import './helpers/temptimerfix.js';


import * as firebase from 'firebase';
const config = {
    apiKey: FIREBASE_API,
    authDomain: FIREBASE_AUTH_DOMAIN,
    databaseURL: FIREBASE_DATABASE_URL,
    projectId: FIREBASE_PROJECT_ID,
    storageBucket: FIREBASE_STORAGE_BUCKET,
    messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
    appId: FIREBASE_APP_ID,
    measurementId: FIREBASE_MEASUREMENT_ID
};
if (!firebase.apps.length)
  firebase.initializeApp(config);

import * as Amplitude from 'expo-analytics-amplitude';
Amplitude.initialize(AMPLITUDE_API);


//Add a loading screen
/*const authLoading = createStackNavigator({
  AuthLoading: {
    screen: AuthLoading
  }
})*/



const home = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen
  },
  Category: {
    screen: CategoryScreen
  }
})

//Tab navigator with home screen, add tip screen, and profile screen
const appNav = createBottomTabNavigator(
  {
    Home: {
      screen: home,
    },
    Explore: {
      screen: ExploreScreen
    },
    AddTip: {
      screen: AddTipScreen
    },
    Notification: {
      screen: NotificationScreen
    },
    Profile: {
      screen: ProfileScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        //creates the icons for the tabs
        switch(routeName) {
          case 'Home': iconName = 'ios-home'; break;
          case 'Explore': iconName = 'md-search'; break;
          case 'AddTip': iconName = 'ios-add'; break;
          case 'Notification': iconName = 'ios-notifications'; break;
          case 'Profile': iconName = 'ios-person'; break;
        }
        // You can return any component that you like here!
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
);

//switch navigator with the loading screen, the app page (with tab navigation), and the sign in/sign up page
const app = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: appNav,
    Auth: SignInStack,
    Tutorial: Tutorial
  },
  {
    initialRouteName: 'AuthLoading'
  }
);

const AppContainer = createAppContainer(app);

export default class TipbitsApp extends React.Component {

  // componentDidMount() {
  //   this.registerForPushNotificationsAsync();
  //   this._notificationSubscription = Notifications.addListener(this._handleNotification);
  //
  // }
  //
  // // handles the notification
  // _handleNotification = notification => {
  //   if(Platform.OS === 'android')
  //     Notifications.dismissAllNotificationsAsync();
  //   if(notification.origin === 'selected' && this.navigator) {
  //     const navigateAction = NavigationActions.navigate({
  //       routeName: 'App',
  //       action: NavigationActions.navigate({ routeName: 'Notification' }),
  //     });
  //     this.navigator.dispatch(navigateAction);
  //   }
  //   else {
  //     Vibration.vibrate();
  //   }
  // };
  //
  // componentWillUnmount() {
  //   this._notificationSubscription.remove();
  // }

  render() {
    return (
      <AppContainer
        ref={nav => {
          this.navigator = nav;
        }}
      />
    )
  }
}
